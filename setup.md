### установка

1. установить cuda 12.1: https://developer.nvidia.com/cuda-12-1-1-download-archive?target_os=Windows&target_arch=x86_64
2. установить anaconda: https://www.anaconda.com/download/
2.1. добавить в PATH (не помню, вроде .../anaconda3/bin/ но могу ошибаться)
3. создать окружение и установить либы:

conda create -n ml python=3.11 -y

conda activate ml

pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu121

4. проверить, доступна ли куда:

python test_cuda.py

### обучение модели

5. NN training:
5.1. установить либы

pip install datasets scipy

5.2. скачать датасет (7-8 гигабайт)

git clone https://huggingface.co/datasets/artyomboyko/common_voice_15_0_RU data/common_voice_15_0_RU/

5.3. активировать окружение (если не активировано еще)

conda activate ml

5.4. запустить main.py - оно само начнет обучать

python main.py

### описание main.py

в общем при каждом запуске модель создаться заново (это минус, но пока пусть будет, ладно уже...)
каждая эпоха модели будет сохраняться в папку models_files/время_запуска/файл_модели
и там еще результаты тестов вроде с этими моделями будут сохраняться

поменять количество эпох в конце main.py там переменная epochs

# ГЛАВНОЕ
ща суть не в том, чтобы получить супер-пупер крутую модель,
а чтобы убедиться в том обучается и построена правильно.
тогда уже можно будет ее улучшать в дальнейшем.

import os
import time
import torch
import torchaudio

from typing import Literal

from datasets import load_dataset
from datasets import DatasetDict
from scipy.signal import resample
from torch import nn
from torch.utils.data import DataLoader

from metrics import cer, wer
from utils import get_device
from utils import set_seeds


class TextTransform:
    def __init__(self, language: Literal['ru'] = 'ru'):
        chars_list = (
            ' ', # space
            "'", '"', '«', '»',
            '.', ',', '!', '?',
            ':', ';',
            '-', # minus
            '—', # dashx
            '(', ')',
            'а', 'б', 'в', 'г', 'д',
            'е', 'ё', 'ж', 'з', 'и',
            'й', 'к', 'л', 'м', 'н',
            'о', 'п', 'р', 'с', 'т',
            'у', 'ф', 'х', 'ц', 'ч',
            'ш', 'щ', 'ъ', 'ы', 'ь',
            'э', 'ю', 'я',
            '0', '1', '2', '3', '4',
            '5', '6', '7', '8', '9',
        )
        self.char_map = {}
        self.index_map = {}

        for i, char in enumerate(chars_list):
            self.char_map[char] = i
            self.index_map[i] = char

        self.space_idx = self.char_map[' ']
        self.blank_label = i + 1
        self.num_classes = self.blank_label + 1

    def text_to_int(self, text: str) -> list[int]:
        int_sequence = []
        for c in text:
            char_idx = self.char_map.get(c)
            if char_idx == None:
                continue
            int_sequence.append(char_idx)

        return int_sequence

    def int_to_text(self, labels: list[int]) -> str:
        string = [
            self.index_map.get(i, '')
            for i
            in labels
        ]

        return ''.join(string)


dataset_path = './data/common_voice_15_0_RU'
datasets = {
    'train' : load_dataset("parquet", data_dir=dataset_path, split="train"),
    'validated' : load_dataset("parquet", data_dir=dataset_path, split="validated"),
    'test' : load_dataset("parquet", data_dir=dataset_path, split="test"),
}

for data_type in datasets:
    datasets[data_type].set_format(type="torch")


def array_resample(src_array: torch.Tensor, src_sample_rate: int, target_sample_rate: int = 16_000) -> torch.Tensor:
    duration = len(src_array) / src_sample_rate
    target_len = int(duration * target_sample_rate)
    downsampled_array = resample(src_array, target_len)
    downsampled_tensor = torch.tensor(downsampled_array)

    return downsampled_tensor


def data_preprocessing(dataset: DatasetDict, text_transform: TextTransform, data_type: Literal['train', 'validate', 'test']) -> tuple[torch.Tensor, torch.Tensor, list[int], list[int]]:
    spectrograms = []
    labels = []
    input_lengths = []
    label_lengths = []

    for data in dataset:
        src_array = data['path']['array']
        src_sample_rate = data['path']['sampling_rate']
        sentence = data['sentence']

        sentence = sentence.lower()
        label = text_transform.text_to_int(sentence)
        label = torch.tensor(label, dtype=torch.float32)
        labels.append(label)

        downsampled_waveform = array_resample(src_array=src_array, src_sample_rate=src_sample_rate, target_sample_rate=16_000)
        if data_type == 'train':
            transformer = nn.Sequential(
                torchaudio.transforms.MelSpectrogram(sample_rate=16_000, n_mels=128),
                # torchaudio.transforms.FrequencyMasking(freq_mask_param=15),
                # torchaudio.transforms.TimeMasking(time_mask_param=35),
            )
        else:
            transformer = torchaudio.transforms.MelSpectrogram(sample_rate=16_000, n_mels=128)

        spec = transformer(downsampled_waveform)
        spec = spec.squeeze(0).transpose(0, 1)
        spectrograms.append(spec)

        input_lengths.append(spec.shape[0]//2)
        label_lengths.append(len(label))

    spectrograms = nn.utils.rnn.pad_sequence(spectrograms, batch_first=True).unsqueeze(1).transpose(2, 3)
    labels = nn.utils.rnn.pad_sequence(labels, batch_first=True)

    return spectrograms, labels, input_lengths, label_lengths


class CNNLayerNorm(nn.Module):
    def __init__(self, n_feats):
        super().__init__()
        self.layer_norm = nn.LayerNorm(n_feats)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        # x (batch, channel, feature, time)
        x = x.transpose(2, 3).contiguous() # (batch, channel, time, feature)
        x = self.layer_norm(x)
        return x.transpose(2, 3).contiguous() # (batch, channel, feature, time)


class ResidualCNN(nn.Module):
    def __init__(self,
                 in_channels, out_channels, kernel,
                 stride, dropout, n_feats):
        super().__init__()

        self.layers = nn.Sequential(
            CNNLayerNorm(n_feats=n_feats),
            nn.GELU(),
            nn.Dropout(dropout),
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel, stride=stride, padding=kernel//2),
            CNNLayerNorm(n_feats=n_feats),
            nn.GELU(),
            nn.Dropout(dropout),
            nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=kernel, stride=stride, padding=kernel//2)
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        residual = x  # (batch, channel, feature, time)
        x = self.layers(x)
        x += residual
        return x


class BidirectionalGRU(nn.Module):
    def __init__(self, rnn_dim, hidden_size, dropout, batch_first):
        super().__init__()

        self.layer_norm = nn.LayerNorm(rnn_dim)
        self.gelu = nn.GELU()
        self.BiGRU = nn.GRU(input_size=rnn_dim, hidden_size=hidden_size,
                            num_layers=1, batch_first=batch_first, bidirectional=True)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.layer_norm(x)
        x = self.gelu(x)
        x, _ = self.BiGRU(x)
        x = self.dropout(x)
        return x


class SpeechRecognitionModel(nn.Module):
    def __init__(self, text_transform: TextTransform, hidden_layers, n_cnn_layers, dropout, n_rnn_layers, rnn_dim, n_feats):
        super().__init__()
        n_classes = text_transform.num_classes
        n_feats = n_feats//2
        self.cnn = nn.Conv2d(in_channels=1,
                             out_channels=hidden_layers,
                             kernel_size=3,
                             stride=2,
                             padding=3//2)

        self.rescnn_layers = nn.Sequential(*[
            ResidualCNN(in_channels=hidden_layers,
                        out_channels=hidden_layers,
                        kernel=3,
                        stride=1,
                        dropout=dropout,
                        n_feats=n_feats)
            for layer in range(n_cnn_layers)
        ])

        self.fully_connected = nn.Linear(in_features=n_feats * hidden_layers,
                                         out_features=rnn_dim)

        self.birnn_layers = nn.Sequential(*[
            BidirectionalGRU(rnn_dim=rnn_dim if i==0 else rnn_dim*2,
                             hidden_size=rnn_dim,
                             dropout=dropout, batch_first=i==0)
            for i in range(n_rnn_layers)
        ])

        self.classifier = nn.Sequential(
            nn.Linear(rnn_dim*2, rnn_dim),
            nn.GELU(),
            nn.Dropout(dropout),
            nn.Linear(rnn_dim, n_classes)
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.cnn(x)
        x = self.rescnn_layers(x)
        sizes = x.size()
        x = x.view(sizes[0], sizes[1] * sizes[2], sizes[3])  # (batch, feature, time)
        x = x.transpose(1, 2) # (batch, time, feature)
        x = self.fully_connected(x)
        x = self.birnn_layers(x)
        x = self.classifier(x)

        return x


def greedy_decoder(output, labels, label_lengths: list[int], text_transform: TextTransform, collapse_repeated = True):
	blank_label = text_transform.num_classes
	arg_maxes = torch.argmax(output, dim=2)
	decodes = []
	targets = []

	for i, args in enumerate(arg_maxes):
		decode = []
		# targets.append(text_transform.int_to_text(labels[i][:label_lengths[i]].tolist()))
		targets.append(text_transform.int_to_text(labels[i].tolist()))
		for j, index in enumerate(args):
			if index != blank_label:
				if collapse_repeated and j != 0 and index == args[j -1]:
					continue
				decode.append(index.item())
		decodes.append(text_transform.int_to_text(decode))

	return decodes, targets

def train(model: nn.Module, dataloader: DataLoader, optimizer: torch.optim.Optimizer, criterion: nn.Module, epoch: int, device: torch.device):
    data_len = len(dataloader)
    dataset_len = len(dataloader.dataset)

    model.train()
    for batch_idx, data in enumerate(dataloader):
        spectrograms, labels, input_lengths, label_lengths = data
        spectrograms = spectrograms.to(device)
        labels = labels.to(device)

        output = model(spectrograms)
        output = torch.log_softmax(output, dim=2)
        output = output.transpose(0, 1)

        criterion_fn_name = criterion._get_name()
        match criterion_fn_name:
            case 'CTCLoss':
                loss = criterion(output, labels, input_lengths, label_lengths)
            case _:
                raise Exception(f'Unknown criterion function: {criterion_fn_name}')

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch_idx % 100 == 0 or batch_idx == data_len:
            print(f'[Train] Epoch: {epoch} | [{batch_idx * len(spectrograms)}/{dataset_len} ({100. * batch_idx / data_len:.0f}%)] Loss: {loss.item():.6f}')


def test(model: nn.Module, dataloader: DataLoader, optimizer: torch.optim.Optimizer, criterion: nn.Module, epoch: int, device: torch.device, text_transform: TextTransform):
    model.eval()

    test_loss = 0
    test_cer = []
    test_wer = []

    with torch.no_grad():
        for batch_idx, data in enumerate(dataloader):
            spectrograms, labels, input_lengths, label_lengths = data
            spectrograms, labels = spectrograms.to(device), labels.to(device)

            output = model(spectrograms)  # (batch, time, n_class)
            output = torch.log_softmax(output, dim=2)
            output = output.transpose(0, 1) # (time, batch, n_class)

            criterion_fn_name = criterion._get_name()
            match criterion_fn_name:
                case 'CTCLoss':
                    loss = criterion(output, labels, input_lengths, label_lengths)
                case _:
                    raise Exception(f'Unknown criterion function: {criterion_fn_name}')

            test_loss += loss.item() / len(dataloader)

            decoded_preds, decoded_targets = greedy_decoder(output.transpose(0, 1), labels, label_lengths, text_transform)
            for j in range(len(decoded_preds)):
                test_cer.append(cer(decoded_targets[j], decoded_preds[j]))
                test_wer.append(wer(decoded_targets[j], decoded_preds[j]))

    avg_cer = sum(test_cer)/len(test_cer)
    avg_wer = sum(test_wer)/len(test_wer)

    print(f'Target (0) : {decoded_targets[0]}')
    print(f'Predict (0): {decoded_preds[0]}')
    print(f'[Test]  Epoch: {epoch} | Average loss: {test_loss:.4f}, Average CER: {avg_cer:4f} Average WER: {avg_wer:.4f}')
    return epoch, test_loss, avg_cer, avg_wer, decoded_targets, decoded_preds


def main(epochs):
    text_transform = TextTransform(language="ru")
    hyperparameters = {
        "hidden_layers": 128,
        "n_cnn_layers": 2,
        "dropout": 0.2,
        "n_rnn_layers": 2,
        "rnn_dim": 512,
        "n_feats": 128,
    }

    device = get_device('cuda') if torch.cuda.is_available() else get_device('cpu')
    model = SpeechRecognitionModel(text_transform, **hyperparameters).to(device=device)

    learning_rate = 0.05
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    criterion = nn.CTCLoss(blank=text_transform.blank_label, reduction='mean', zero_infinity=True).to(device=device)

    train_dataset_common_voice = datasets['train']
    validated_dataset_common_voice = datasets['validated']
    test_dataset_common_voice = datasets['test']

    # _, train_dataset_common_voice = train_dataset_common_voice.train_test_split(test_size=0.01, seed=42).values()
    _, test_dataset_common_voice = test_dataset_common_voice.train_test_split(test_size=0.001, seed=42).values()

    batch_size = 32
    set_seeds()
    train_dataloader = DataLoader(dataset=train_dataset_common_voice,
                                batch_size=batch_size,
                                shuffle=True,
                                collate_fn=lambda x: data_preprocessing(x, text_transform, 'train'))
    set_seeds()
    test_dataloader = DataLoader(dataset=test_dataset_common_voice,
                                batch_size=batch_size,
                                shuffle=True,
                                collate_fn=lambda x: data_preprocessing(x, text_transform, 'test'))

    timestamp = time.time()
    MODEL_DIR = 'models_files/'
    if not os.path.exists(MODEL_DIR):
        os.mkdir(MODEL_DIR)
    MODEL_DIR += str(timestamp)
    if not os.path.exists(MODEL_DIR):
        os.mkdir(MODEL_DIR)


    for epoch in range(1, epochs + 1):
        train(model, train_dataloader, optimizer, criterion, epoch, device)
        result = test(model, train_dataloader, optimizer, criterion, epoch, device, text_transform)

        MODEL_SAVE_PATH = f'{MODEL_DIR}/model_ru_epoch_{epoch}.pt'
        torch.save(obj=model.state_dict(), f=MODEL_SAVE_PATH)

        TEST_RESULT_PATH = f'{MODEL_DIR}/model_ru_epoch_{epoch}_test.txt'
        with open(TEST_RESULT_PATH, mode='w') as f:
            result = [str(r) + '\n' for r in result]
            f.writelines(result)


if __name__ == '__main__':
    epochs = 100
    main(epochs=epochs)

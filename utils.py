import torch
from typing import Literal


def accuracy(y_true: torch.Tensor, y_pred: torch.Tensor) -> float:
    correct = torch.eq(y_true, y_pred).sum().item()
    acc = (correct / len(y_pred)) * 100
    return acc


def set_seeds(seed: int = 302):
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.mps.manual_seed(seed)


def get_device(device = Literal['cuda', 'mps', 'cpu']) -> torch.device:
    match device:
        case 'cuda':
            return torch.device('cuda')
        case 'mps':
            return torch.device('mps')
        case 'cpu':
            return torch.device('cpu')
        case _:
            device = 'cuda' if torch.cuda.is_available() else \
                     'mps' if torch.backends.mps.is_available() else \
                     'cpu'
            return torch.device(device)


class IterMeter(object):
    """keeps track of total iterations"""
    def __init__(self):
        self.val = 0

    def step(self):
        self.val += 1

    def get(self):
        return self.val
